<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.Login.DbOperationDao"%>
<link rel="stylesheet" type="text/css" href="index.css">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert Data into Database</title>
</head>
<body>
<h1>Contact us</h1>
<h3>Fill the HTML Form</h3>
<div>
  <form action="DbOperationCall" method="post">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" placeholder="Your name..">
    <label for="email">Email</label>
    <input type="email" id="email" name="email" placeholder="Your email..">
    <label for="message">Message</label>
    <input type="text" id="message" name="message">
    <input type="submit" value="Submit">
  </form>
</div>
</body>
</html>